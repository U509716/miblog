
const URL = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts?apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs"
const URLBASE = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts/"
const apiKey = "?apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs"
var response;

function obtenerPosts(){

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  mostrarPost();
};

function mostrarPost() {

  var tabla = document.getElementById("tablaPosts");
//Para Borrar la Tabla
  var tablaHeaderRowCount = 1;
  var rowCount = tabla.rows.length;
  for (var i = tablaHeaderRowCount; i < rowCount; i++) {
      tabla.deleteRow(tablaHeaderRowCount);
      };
//Para Borrar la Tabla
  var user = sessionStorage["usuario"]
  console.log(user);
  for (var i = 0; i < response.length; i++) {
    //alert(response[i].titulo);
    var fila = tabla.insertRow(i+1);
    var celdaTitulo = fila.insertCell(0);
    var celdaTexto = fila.insertCell(1);
    var celdaAutor = fila.insertCell(2);
    var celdaId = fila.insertCell(3);
    var celdaOperaciones = fila.insertCell(4);

    celdaTitulo.innerHTML = response[i].titulo;
    celdaTexto.innerHTML = response[i].texto;
    if (response[i].autor != undefined)
    {
      celdaAutor.innerHTML = response[i].autor.nombre + " " + response[i].autor.apellido;
    }
    else {
      celdaAutor.innerHTML = "Anonimus"
    };
    console.log(celdaAutor);
    celdaId.innerHTML = response[i]._id.$oid;

    if (user == undefined)
    {
      celdaOperaciones.innerHTML =
        '<button class="btn btn-primary" onclick=\'seleccionarPost("' + celdaId.innerHTML + '")\'; disabled>Ver Post</button>'+
        '<button class="btn btn-primary" onclick=\'modificarPost("' + celdaId.innerHTML + '")\'; disabled>Actualiz.</button>'+
        '<button class="btn btn-primary" onclick=\'deletePost("' + celdaId.innerHTML + '")\'; disabled>Borrar</button>';
    }
    else {
      if (user == response[i].autor.nombre)
      {
        celdaOperaciones.innerHTML =
          '<button class="btn btn-primary" onclick=\'seleccionarPost("' + celdaId.innerHTML + '")\';>Ver Post</button>'+
          '<button class="btn btn-primary" onclick=\'modificarPost("' + celdaId.innerHTML + '")\';>Actualiz.</button>'+
          '<button class="btn btn-primary" onclick=\'deletePost("' + celdaId.innerHTML + '")\';>Borrar</button>';

      }
      else {
        celdaOperaciones.innerHTML =
          '<button class="btn btn-primary" onclick=\'seleccionarPost("' + celdaId.innerHTML + '")\';>Ver Post</button>'+
          '<button class="btn btn-primary" onclick=\'modificarPost("' + celdaId.innerHTML + '")\'; disabled>Actualiz.</button>'+
          '<button class="btn btn-primary" onclick=\'deletePost("' + celdaId.innerHTML + '")\'; disabled>Borrar</button>';
      };
    };
  };
};

function anadirPost() {
  var peticion = new XMLHttpRequest();
  var titulo = document.getElementById("titulo").value;
  var texto = document.getElementById("texto").value;
  var autor = sessionStorage["usuario"]

  console.log(titulo);
  console.log(texto);
  console.log(autor);

  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  //peticion.send('{"titulo":"Nuevo Post desde Atom", "texto":"Nuevo texto desde Atom", "autor":{"nombre":"Armandito", "apellido":"Casitas"}}');
  peticion.send('{"titulo":"' + titulo + '", "texto": "' + texto + '", "autor":{"nombre":"' + autor + '", "apellido":"' + autor + '" }}');

  var spanMensaje = document.getElementById("mensaje");
  var respuesta = JSON.parse(peticion.responseText);
  if (peticion.status=="200" )
    {
      spanMensaje.innerText = "Alta correcta";
    }
  else
    {
      spanMensaje.innerText = "Alta incorrecta. <Vuelva a intentarlo más tarde>";
    }
};

function deletePost(numero) {
  sessionStorage["seleccionado"]=numero;
  window.location = '/deletePost/1';
};

function deletePostConfirma(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = URLBASE;
      URLItem += id;
      URLItem += apiKey;
  peticion.open("DELETE", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  var spanMensaje = document.getElementById("mensaje");
  var respuesta = JSON.parse(peticion.responseText);
  if (peticion.status=="200" )
    { spanMensaje.innerText = "Borrado Correcto"; }
  else
    { spanMensaje.innerText = "Borrado incorrecta. <Vuelva a intentarlo más tarde>"; }
};

function seleccionarPost(numero){
  sessionStorage["seleccionado"]=numero;
  window.location = '/detallePost/1';
};

function buscarDetallesPosts(id) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == id)
    {
      // mostrar detalle
      document.getElementById("h1").innerHTML = id;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      document.getElementById("h3").innerHTML = posts[i].texto;


      if (posts[i].autor != undefined)
      {
        document.getElementById("h4").innerHTML = posts[i].autor.nombre + " " + posts[i].autor.apellido;
      }
      else {
        document.getElementById("h4").innerHTML = "Anonimus"
      };

      break;
    }
  }
};

// Para trastear
function modificarPost(numero){
  sessionStorage["seleccionado"]=numero;
  window.location = '/modificarPost/1';
};

function buscarDetallesPosts2(id) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == id)
    {
      // mostrar detalle
      document.getElementById("h1").innerHTML = id;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      document.getElementById("h3").innerHTML = posts[i].texto;
      if (posts[i].autor != undefined)
      {
        // document.getElementById("h4").innerHTML = posts[i].autor.nombre + " " + posts[i].autor.apellido;
        document.getElementById("h4").innerHTML = "Autor actual: " + posts[i].autor.nombre + " " + posts[i].autor.apellido + " - Nuevo Autor: " + sessionStorage["usuario"] ;
      }
      else {
        document.getElementById("h4").innerHTML = "Sin Autor" + " - Nuevo Autor: " + sessionStorage["usuario"] ;
      };

      document.getElementById("m1").innerHTML = "Titulo actual: " + posts[i].titulo;
      document.getElementById("m2").innerHTML = "Texto actual: " + posts[i].texto;

      break;
    }
  }
};

function actualizarPost(id) {
  //sessionStorage["seleccionado"]=id;
  var peticion = new XMLHttpRequest();
  var URLItem = URLBASE;
      URLItem += id;
      URLItem += apiKey;

  var titulo = document.getElementById("titulo").value;
  var texto = document.getElementById("texto").value;
  var autor = sessionStorage["usuario"]

  console.log(titulo);
  console.log(texto);
  console.log(autor);

  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  //peticion.send('{"titulo":"Titulo cambiad34"}');
  peticion.send('{"titulo":"' + titulo + '", "texto": "' + texto + '", "autor":{"nombre":"' + autor + '", "apellido":"' + autor + '" }}');
  // , "autor.nombre":"' + autor + '", "autor.apellido":"' + autor + '" }');

  var spanMensaje = document.getElementById("mensaje");
  var respuesta = JSON.parse(peticion.responseText);
  if (peticion.status=="200" )
    {
      spanMensaje.innerText = "Modificación correcta";
  }
  else
  {
      spanMensaje.innerText = "Modificación incorrecta. <Vuelva a intentarlo más tarde>";
  }

};

function buscarDetallesPosts3(id) {
  document.getElementById("h4").innerHTML = "Autor: " + sessionStorage["usuario"] ;
};

function buscarDetallesPosts4(id) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == id)
    {
      // mostrar detalle
      document.getElementById("m1").innerHTML = "Titulo actual: " + posts[i].titulo;
      document.getElementById("m2").innerHTML = "Texto actual: " + posts[i].texto;

      if (posts[i].autor != undefined)
      {
        document.getElementById("h4").innerHTML = "Autor: " + posts[i].autor.nombre + " " + posts[i].autor.apellido;
      }
      else {
        document.getElementById("h4").innerHTML = "Autor: Anonimus"
      };
      break;
    }
  }
};

function controlblog(){
  var user = sessionStorage["usuario"];
  var botonNuevo=document.getElementById("btnNuevoPost")
  var botonMios=document.getElementById("btnMisPost")
  if (user != undefined)
    {
      document.getElementById("h5").innerHTML = "Usuario: " + user;
      document.getElementById("h6").innerHTML = "Email: " + sessionStorage["correo"];
      botonNuevo.disabled = false
      botonMios.disabled = false
    }
  else {
      document.getElementById("h5").innerHTML = "Usuario Invitado: Solo Consulta Básica";
      document.getElementById("h6").innerHTML = "";
      botonNuevo.disabled = true
      botonMios.disabled = true
      }
};

function obtenerMisPosts(){
  const URLBASE2 = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts?q="
  const URLBASE3 = '{"autor":{"nombre":"'
  var user = sessionStorage["usuario"]
  const URLBASE4 = '","apellido":"'
  const apiKey2 = '"}}&apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs'

  var URLItem2 = URLBASE2;
      URLItem2 += URLBASE3;
      URLItem2 += user;
      URLItem2 += URLBASE4;
      URLItem2 += user;
      URLItem2 += apiKey2;
  console.log(URLItem2);
  //https://api.mlab.com/api/1/databases/bootcamp/collections/posts?q={"autor": "Bob"}&apiKey=xxxxxx

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem2, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  mostrarPost();
};
