var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

describe ("Pruebas Sencillas", function() {
  it('Test suma', function() {
    expect(9+4).to.equal(13);
  });
});

describe ("Pruebas de Red", function() {
  it('Test Internet', function(done) {
    request.get("http://www.forocoches.com",
                function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
    });
  });

  it('Test MiBlog', function(done) {
    request.get("http://localhost:8082",
                function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
    });
  });

  it('Test del Body de MiBlog', function(done) {
    request.get("http://localhost:8082",
                function(error, response, body) {
                  expect(body).to.contains('Bienvenido a mi Blog');
                  done();
    });
  });
});

describe ("Pruebas contenido HTML", function() {
  it('Test H1', function() {
    request.get("http://localhost:8082",
                function(error, response, body) {
                console.log(body);
                expect($('body h1')).to.have.text("Bienvenido a mi Blog");

    });
  });
});
