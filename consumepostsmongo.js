
const URL = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts?apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs";
const URLBASE = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts/";
const URLBASE2 = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts?";
const apiKey = "?apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs";
const apiKey2 = '&apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs';
var response;
var origen;


const URLPOSTGRE = "http://postgrebc9688.cloudapp.net:3000/api/login";
const URLPOSTGRE2 = "http://postgrebc9688.cloudapp.net:3000/api/usuarios";

const sinimagen = "../sin_imagen.jpg";
const comida = "/Comida.jpg";
const S3AWS = "https://s3-eu-west-1.amazonaws.com/bootcamparmando/AmazonS3.png";


<!--  Control inicial de la Ventana Inicio-->
function controlblog(){
  var user = sessionStorage["usuario"];
  var botonNuevo=document.getElementById("btnNuevoPost")
  var botonMios=document.getElementById("btnMisPost")
  if (user != undefined)
    {
      ContarLogado();
      document.getElementById("h5").innerHTML = "Usuario: " + user + " (Posts: "+sessionStorage["Numero"] +")";
      document.getElementById("h6").innerHTML = "Email: " + sessionStorage["correo"];
      document.getElementById("usuactu").innerHTML = "Usuario Actual: " + user;
      document.getElementById("usuactureg").innerHTML = "Usuario Actual: " + user;

      botonNuevo.disabled = false;
      botonMios.disabled = false;
      btnadmin.disabled = false;
    }
  else {
      document.getElementById("h5").innerHTML = "Usuario Invitado: Solo Consulta Básica";
      document.getElementById("h6").innerHTML = "";
      botonNuevo.disabled = true;
      botonMios.disabled = true;
      btnadmin.disabled = true;
      solousu.disabled=true;
      solousu.title="Debes Logarte Primero";
  };
  if (sessionStorage["Filtro"] == "misposts") {
    document.getElementById("solousu").checked=true;

  } else {
    document.getElementById("todousu").checked=true;
    sessionStorage["Filtro"] = "todosposts"
  };
  if (sessionStorage["Filtro2"] != undefined) {
    document.getElementById("tiempo").value = sessionStorage["Filtro2"];
  } else {
    sessionStorage["Filtro2"] = '0';
  };
  if (sessionStorage["Filtro3"] != undefined) {
    switch (sessionStorage["Filtro3"]){
          case("1"):
            document.getElementById("Baja").checked=true;
            break;
          case("2"):
              document.getElementById("Media").checked=true;
              break;
          case("3"):
            document.getElementById("Alta").checked=true;
            break;
          default:
            document.getElementById("Todas").checked=true;
            break;
          };
  } else {
    sessionStorage["Filtro3"] = '0';
  };
  obtener6Posts();
};
// Obsoleto
function obtenerPosts(){
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  sessionStorage["Filtro"] = "todosposts";
  mostrarPost();
};

function mostrarPost() {
  var user = sessionStorage["usuario"]
  console.log(user);
  console.log(response.length)
  for (var i = 0; i < response.length; i++) {
//Pruebas
    if (i == 0){
      console.log("Rellena post 1");
      document.getElementById("id1").innerHTML = response[i]._id.$oid;
      document.getElementById("boton1").onclick = seleccionarPostDetalle1;
      console.log(document.getElementById("id1").innerHTML);
      //boton1.innerHTML ='<button class="btn btn-primary" onclick=\'seleccionarPost("' + response[i]._id.$oid + '")\';>Ver Post</button>'
      document.getElementById("tit1").innerHTML = response[i].titulo;

      //var document.getElementById("texto1").innerHTML = response[i].texto.replace(/\n/g, "<br>");
      if (response[i].texto.length > 40)
       {document.getElementById("texto1").innerHTML = response[i].texto.substr(0, 40) + "..."; }
      else {document.getElementById("texto1").innerHTML = response[i].texto; };

      if ((response[i].imagen != undefined) && (response[i].imagen != "")) {
        document.getElementById("imagen1").src = response[i].imagen;
        document.getElementById("imagen1").onerror = function() {sinimagen1("imagen1")};

      }
      else { document.getElementById("imagen1").src = sinimagen; };
    };

    if (i == 1){
      document.getElementById("id2").innerHTML = response[i]._id.$oid;
      document.getElementById("boton2").onclick = seleccionarPostDetalle2;
      document.getElementById("tit2").innerHTML = response[i].titulo;
      //document.getElementById("texto2").innerHTML = response[i].texto;
      if (response[i].texto.length > 40)
       {document.getElementById("texto2").innerHTML = response[i].texto.substr(0, 40) + "..."; }
      else {document.getElementById("texto2").innerHTML = response[i].texto; };

      if ((response[i].imagen != undefined) && (response[i].imagen != "")) {
        document.getElementById("imagen2").src = response[i].imagen;
        document.getElementById("imagen2").onerror = function() {sinimagen1("imagen2")};
      }
      else { document.getElementById("imagen2").src = sinimagen; };
    };
    if (i == 2){
      document.getElementById("id3").innerHTML = response[i]._id.$oid;
      document.getElementById("boton3").onclick = seleccionarPostDetalle3;
      document.getElementById("tit3").innerHTML = response[i].titulo;
      //document.getElementById("texto3").innerHTML = response[i].texto;
      if (response[i].texto.length > 40)
       {document.getElementById("texto3").innerHTML = response[i].texto.substr(0, 40) + "..."; }
      else {document.getElementById("texto3").innerHTML = response[i].texto; };
      console.log(response[i].imagen)
      console.log("La imagen 3")
      if ((response[i].imagen != undefined) && (response[i].imagen != "")) {
        document.getElementById("imagen3").src = response[i].imagen;
        document.getElementById("imagen3").onerror = function() {sinimagen1("imagen3")};
      }
      else { document.getElementById("imagen3").src = sinimagen; };
    };
    if (i == 3){
      document.getElementById("id4").innerHTML = response[i]._id.$oid;
      document.getElementById("boton4").onclick = seleccionarPostDetalle4;
      document.getElementById("tit4").innerHTML = response[i].titulo;
      //document.getElementById("texto4").innerHTML = response[i].texto;
      if (response[i].texto.length > 40)
       {document.getElementById("texto4").innerHTML = response[i].texto.substr(0, 40) + "..."; }
      else {document.getElementById("texto4").innerHTML = response[i].texto; };

      console.log(response[i].imagen)
      console.log("La imagen 4")
      if ((response[i].imagen != undefined) && (response[i].imagen != "")) {
        document.getElementById("imagen4").src = response[i].imagen;
        document.getElementById("imagen4").onerror = function() {sinimagen1("imagen4")};
      }
      else {document.getElementById("imagen4").src = sinimagen; };
    };
    if (i == 4){
      document.getElementById("id5").innerHTML = response[i]._id.$oid;
      document.getElementById("boton5").onclick = seleccionarPostDetalle5;
      document.getElementById("tit5").innerHTML = response[i].titulo;
      //document.getElementById("texto5").innerHTML = response[i].texto;
      if (response[i].texto.length > 40)
       {document.getElementById("texto5").innerHTML = response[i].texto.substr(0, 40) + "..."; }
      else {document.getElementById("texto5").innerHTML = response[i].texto; };

      if ((response[i].imagen != undefined) && (response[i].imagen != "")) {
        document.getElementById("imagen5").src = response[i].imagen;
        document.getElementById("imagen5").onerror = function() {sinimagen1("imagen5")};
      }
      else { document.getElementById("imagen5").src = sinimagen; };
    };
    if (i == 5){
      document.getElementById("id6").innerHTML = response[i]._id.$oid;
      document.getElementById("boton6").onclick = seleccionarPostDetalle6;
      document.getElementById("tit6").innerHTML = response[i].titulo;
      //document.getElementById("texto6").innerHTML = response[i].texto;
      if (response[i].texto.length > 40)
       {document.getElementById("texto6").innerHTML = response[i].texto.substr(0, 40) + "..."; }
      else {document.getElementById("texto6").innerHTML = response[i].texto; };

      if ((response[i].imagen != undefined) && (response[i].imagen != "")) {
        document.getElementById("imagen6").src = response[i].imagen;
        document.getElementById("imagen6").onerror = function() {sinimagen1("imagen6")};
      }
      else { document.getElementById("imagen6").src = sinimagen; };
    };

    if (user != undefined) {
      boton1.disabled = false;
      boton2.disabled = false;
      boton3.disabled = false;
      boton4.disabled = false;
      boton5.disabled = false;
      boton6.disabled = false;
    } else {
      boton1.disabled = true;
      boton2.disabled = true;
      boton3.disabled = true;
      boton4.disabled = true;
      boton5.disabled = true;
      boton6.disabled = true;
      boton1.title="Solo Usuarios Logados";
      boton2.title="Solo Usuarios Logados";
      boton3.title="Solo Usuarios Logados";
      boton4.title="Solo Usuarios Logados";
      boton5.title="Solo Usuarios Logados";
      boton6.title="Solo Usuarios Logados";
    };
  }; //fin de for
  if (i>6){i=6};
  console.log(i);
  switch (i){
        case(6):
          post1.style.display='initial';
          post2.style.display='initial';
          post3.style.display='initial';
          post4.style.display='initial';
          post5.style.display='initial';
          post6.style.display='initial';
          break;
        case(5):
          post1.style.display='initial';
          post2.style.display='initial';
          post3.style.display='initial';
          post4.style.display='initial';
          post5.style.display='initial';
          post6.style.display='none';
          break;
        case(4):
          post1.style.display='initial';
          post2.style.display='initial';
          post3.style.display='initial';
          post4.style.display='initial';
          post5.style.display='none';
          post6.style.display='none';
          break;
        case(3):
          post1.style.display='initial';
          post2.style.display='initial';
          post3.style.display='initial';
          post4.style.display='none';
          post5.style.display='none';
          post6.style.display='none';
          break;
        case(2):
          post1.style.display='initial';
          post2.style.display='initial';
          post3.style.display='none';
          post4.style.display='none';
          post5.style.display='none';
          post6.style.display='none';
          break;
        case(1):
          post1.style.display='initial';
          post2.style.display='none';
          post3.style.display='none';
          post4.style.display='none';
          post5.style.display='none';
          post6.style.display='none';
          break;
        default:
          post1.style.display='none';
          post2.style.display='none';
          post3.style.display='none';
          post4.style.display='none';
          post5.style.display='none';
          post6.style.display='none';
          alert("No se han obtenido resultados")
          break;
      };
};

function anadirPost() {
  var peticion = new XMLHttpRequest();
  var titulo = document.getElementById("titulo").value;
  var texto = document.getElementById("texto").value;
  var ingredientes = document.getElementById("ingredientes").value;
  var preparacion = document.getElementById("preparacion").value;
  var imagen = document.getElementById("imagen").value;
  var autor = sessionStorage["usuario"]
  var tiempo = document.getElementById("tiempo").value;
  var tiemponum = parseInt(document.getElementById("tiempo").value);

  var eledificultad = document.getElementsByName("dificultad");
  for(var i=0; i < eledificultad.length; i++) {
    if (eledificultad[i].checked) {
      var dificultad = eledificultad[i].value;
      break;
    };
  };

  console.log(titulo);
  console.log(texto);
  console.log(autor);

  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  //peticion.send('{"titulo":"Nuevo Post desde Atom", "texto":"Nuevo texto desde Atom", "autor":{"nombre":"Armandito", "apellido":"Casitas"}}');
  peticion.send('{"titulo":"' + titulo + '", "texto": "' + texto + '", "ingredientes": "' + ingredientes + '", "preparacion": "' + preparacion + '", "imagen": "' + imagen + '", "tiempo": '+tiemponum+',  "dificultad": "' + dificultad + '","autor":{"nombre":"' + autor + '", "apellido":"' + autor + '" }}');

  var spanMensaje = document.getElementById("mensaje");
  var respuesta = JSON.parse(peticion.responseText);

  if (peticion.status=="200" ) {
      spanMensaje.innerText = "Alta correcta.";
      alert("Alta correcta.");
      window.history.back();
      
  }  else    {
      spanMensaje.innerText = "Alta incorrecta. <Vuelva a intentarlo más tarde>";
  };
};

function deletePost(numero) {
  sessionStorage["seleccionado"]=numero;
  window.location = '/deletePost/1';
};
function deletePostDet(){
  var numero = sessionStorage["seleccionado"];
  deletePost(numero);
};

function deletePostConfirma(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = URLBASE;
      URLItem += id;
      URLItem += apiKey;
  peticion.open("DELETE", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  var spanMensaje = document.getElementById("mensaje");
  var respuesta = JSON.parse(peticion.responseText);
  if (peticion.status=="200" )
    { spanMensaje.innerText = "Borrado Correcto";
    alert("Borrado Correcto");
    volverborrar();

  } else
    { spanMensaje.innerText = "Borrado incorrecta. <Vuelva a intentarlo más tarde>"; }
};

function seleccionarPostDetalle1(){
  var idendet = document.getElementById("id1").innerHTML;
  seleccionarPost(idendet);};
function seleccionarPostDetalle2(){
  var idendet = document.getElementById("id2").innerHTML;
  seleccionarPost(idendet);};
function seleccionarPostDetalle3(){
  var idendet = document.getElementById("id3").innerHTML;
  seleccionarPost(idendet);};
function seleccionarPostDetalle4(){
  var idendet = document.getElementById("id4").innerHTML;
  seleccionarPost(idendet);};
function seleccionarPostDetalle5(){
  var idendet = document.getElementById("id5").innerHTML;
  seleccionarPost(idendet);};
function seleccionarPostDetalle6(){
  var idendet = document.getElementById("id6").innerHTML;
  seleccionarPost(idendet);};

function seleccionarPost(numero){
  sessionStorage["seleccionado"]=numero;
  window.location = '/detallePost/1';
};

<!-- Ventana Detalle -->

function buscarDetallesPosts(id) {
  if (sessionStorage["Ventana"] != "admin/detalle") {
      sessionStorage["Ventana"] = "detalle";
  };

  var user = sessionStorage["usuario"];
  document.getElementById("btnModificar").onclick = modificarPostDet;
  document.getElementById("btnBorrar").onclick = deletePostDet;

  var URLID = 'q={"_id":{"$oid":"' + id + '"}}';

  var URLItem3  = URLBASE2;
      URLItem3 += URLID;
      URLItem3 += apiKey2;
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem3, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  console.log(response);
  document.getElementById("h1").innerHTML = id;
  document.getElementById("h2").innerHTML = response[0].titulo;
  //document.getElementById("h3").innerHTML = posts[i].texto;
  document.getElementById("h3").innerHTML = response[0].texto.replace(/\n/g, "<br>");

  if (response[0].ingredientes == undefined)  {
    document.getElementById("ingredientes").innerHTML = "Ingedientes";
  } else {
    document.getElementById("ingredientes").innerHTML = response[0].ingredientes.replace(/\n/g, "<br>");
  };
  if (response[0].preparacion == undefined)  {
    document.getElementById("preparacion").innerHTML = "Preparacion";
  } else {
    document.getElementById("preparacion").innerHTML = response[0].preparacion.replace(/\n/g, "<br>");
  };

  if (response[0].tiempo>0) {
      document.getElementById("tiempo").innerHTML = response[0].tiempo;
  } else {
    document.getElementById("tiempo").innerHTML = "Si definir";
  };
  document.getElementById("dificultad").innerHTML = "Si definir";
  if (response[0].dificultad=="3") {
      document.getElementById("dificultad").innerHTML = "Alta";
  } else {
    if (response[0].dificultad=="2") {
      document.getElementById("dificultad").innerHTML = "Media";
    } else {
      if (response[0].dificultad=="1") {
        document.getElementById("dificultad").innerHTML = "Baja";
      }
    }
  };
  if ((response[0].imagen == undefined) || (response[0].imagen == "") ) {
      document.getElementById("imagendet").src = sinimagen;
  } else {
    document.getElementById("imagendet").src = response[0].imagen;
    document.getElementById("imagendet").onerror = function() {sinimagen1("imagendet")};
  };
  console.log(document.getElementById("imagendet").src);
  if (response[0].autor != undefined) {
    document.getElementById("h4").innerHTML = response[0].autor.nombre + " " + response[0].autor.apellido;
  } else {
    document.getElementById("h4").innerHTML = "Anonimus"
  };
  if (user == response[0].autor.nombre) {
    btnModificar.disabled = false;
    btnBorrar.disabled = false;
  } else {
    btnModificar.disabled = true;
    btnModificar.title="Solo los Autores";
    btnBorrar.disabled = true;
    btnBorrar.title="Solo los Autores";
  };
};

function buscarDetallesPostsx(id) {
  var posts = JSON.parse(sessionStorage["posts"]);
  var user = sessionStorage["usuario"];
  document.getElementById("btnModificar").onclick = modificarPostDet;
  document.getElementById("btnBorrar").onclick = deletePostDet;

  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == id)
    {
      // mostrar detalle
      document.getElementById("h1").innerHTML = id;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      //document.getElementById("h3").innerHTML = posts[i].texto;
      document.getElementById("h3").innerHTML = posts[i].texto.replace(/\n/g, "<br>");

      if (posts[i].ingredientes == undefined)  {
        document.getElementById("ingredientes").innerHTML = "Ingedientes";
      } else {
        document.getElementById("ingredientes").innerHTML = posts[i].ingredientes.replace(/\n/g, "<br>");
      };
      if (posts[i].preparacion == undefined)  {
        document.getElementById("preparacion").innerHTML = "Preparacion";
      } else {
        document.getElementById("preparacion").innerHTML = posts[i].preparacion.replace(/\n/g, "<br>");
      };

      if (posts[i].tiempo>0) {
          document.getElementById("tiempo").innerHTML = posts[i].tiempo;
      } else {
        document.getElementById("tiempo").innerHTML = "Si definir";
      };
      document.getElementById("dificultad").innerHTML = "Si definir";
      if (posts[i].dificultad=="3") {
          document.getElementById("dificultad").innerHTML = "Alta";
      } else {
        if (posts[i].dificultad=="2") {
          document.getElementById("dificultad").innerHTML = "Media";
        } else {
          if (posts[i].dificultad=="1") {
            document.getElementById("dificultad").innerHTML = "Baja";
          }
        }
      };

      if ((posts[i].imagen == undefined) || (posts[i].imagen == "") ) {
          document.getElementById("imagendet").src = sinimagen;
      } else {
        document.getElementById("imagendet").src = posts[i].imagen;
        document.getElementById("imagendet").onerror = function() {sinimagen1("imagendet")};
      };
      console.log(document.getElementById("imagendet").src);
      if (posts[i].autor != undefined) {
        document.getElementById("h4").innerHTML = posts[i].autor.nombre + " " + posts[i].autor.apellido;
      } else {
        document.getElementById("h4").innerHTML = "Anonimus"
      };
      if (user == posts[i].autor.nombre) {
        btnModificar.disabled = false;
        btnBorrar.disabled = false;
      } else {
        btnModificar.disabled = true;
        btnModificar.title="Solo los Autores";
        btnBorrar.disabled = true;
        btnBorrar.title="Solo los Autores";
      };
      break;
    }
  }
};

function modificarPost(numero){
  sessionStorage["seleccionado"]=numero;
  window.location = '/modificarPost/1';
};
function modificarPostDet(){
  var numero = sessionStorage["seleccionado"];
  modificarPost(numero);
};


<!-- Ventana Modificar Posts -->
function buscarDetallesPosts2(id) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == id)
    {
      // mostrar detalle
      document.getElementById("h1").innerHTML = id;
      if (posts[i].autor != undefined)
      {
        // document.getElementById("h4").innerHTML = posts[i].autor.nombre + " " + posts[i].autor.apellido;
        document.getElementById("h4").innerHTML = "Autor actual: " + posts[i].autor.nombre + " " + posts[i].autor.apellido + " - Nuevo Autor: " + sessionStorage["usuario"] ;
      }
      else {
        document.getElementById("h4").innerHTML = "Sin Autor" + " - Nuevo Autor: " + sessionStorage["usuario"] ;
      };

      document.getElementById("m1").innerHTML = "Titulo actual: " + posts[i].titulo;
      document.getElementById("m2").innerHTML = "Texto actual: " + posts[i].texto;
      document.getElementById("m3").innerHTML = "Imagen actual: " + posts[i].imagen;
      document.getElementById("titulo").value = posts[i].titulo;
      document.getElementById("texto").value = posts[i].texto;
      if (posts[i].ingredientes == undefined) {
        document.getElementById("ingredientes").value = "";
        document.getElementById("ingredientes").placeholder = "Ingredientes de la receta";
      }
      else {
        document.getElementById("ingredientes").value = posts[i].ingredientes;
      };
      if (posts[i].preparacion == undefined) {
        document.getElementById("preparacion").value = "";
        document.getElementById("preparacion").placeholder = "Preparación de la receta";
      }
      else {
        document.getElementById("preparacion").value = posts[i].preparacion;
      };
      if (posts[i].imagen == undefined) {
        document.getElementById("imagen").value = "";
        document.getElementById("imagen").placeholder = "url de la imagen";
      }
      else {
        document.getElementById("imagen").value = posts[i].imagen;
      };

      document.getElementById("tiempo").value = posts[i].tiempo;
      console.log("posts[i].dificultad:"+posts[i].dificultad )
      if (posts[i].dificultad == "3") {
        document.getElementById("Alta").checked=true;
        console.log("Activo Alta");
      } else {
        if (posts[i].dificultad == "2") {
          document.getElementById("Media").checked=true;
          console.log("Activo Media");
        } else {
          document.getElementById("Baja").checked=true;
          console.log("Activo Baja");
        }
      };

      break;
    }
  }
};

function actualizarPost(id) {
  //sessionStorage["seleccionado"]=id;
  var peticion = new XMLHttpRequest();
  var URLItem = URLBASE;
      URLItem += id;
      URLItem += apiKey;

  var titulo = document.getElementById("titulo").value;
  var texto = document.getElementById("texto").value;
  var ingredientes = document.getElementById("ingredientes").value;
  var preparacion = document.getElementById("preparacion").value;
  var imagen = document.getElementById("imagen").value;
  var autor = sessionStorage["usuario"]

  var tiemponum = parseInt(document.getElementById("tiempo").value);

  var eledificultad = document.getElementsByName("dificultad");
  for(var i=0; i < eledificultad.length; i++) {
    if (eledificultad[i].checked) {
      var dificultad = eledificultad[i].value
    };
  };

  console.log(titulo);
  console.log(texto);
  console.log(imagen);
  console.log(autor);

  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  //peticion.send('{"titulo":"Titulo cambiad34"}');
  peticion.send('{"titulo":"' + titulo + '", "texto": "' + texto + '", "ingredientes": "' + ingredientes + '", "preparacion": "' + preparacion + '", "imagen": "' + imagen + '", "tiempo": '+tiemponum+' , "dificultad": "' + dificultad + '", "autor":{"nombre":"' + autor + '", "apellido":"' + autor + '" }}');
  // , "autor.nombre":"' + autor + '", "autor.apellido":"' + autor + '" }');

  var spanMensaje = document.getElementById("mensaje");
  var respuesta = JSON.parse(peticion.responseText);
  if (peticion.status=="200" )
    {
      spanMensaje.innerText = "Modificación correcta";
      alert("Modificación correcta");
      if (sessionStorage["Ventana"] == "admin") {
        window.location = '/admin';
      } else {
        window.location = '/detallePost/1';
      };
      //window.history.back();

  }
  else
  {
      spanMensaje.innerText = "Modificación incorrecta. <Vuelva a intentarlo más tarde>";
  }
};

<!-- Ventana Añadir Posts -->
function buscarDetallesPosts3(id) {
  document.getElementById("h4").innerHTML = "Autor: " + sessionStorage["usuario"] ;
  origen = getQueryVariable('origen');
  console.log(origen);
  };
function volveranadir() {
  console.log(origen);
  if (origen == 'a') {
    window.location = '/admin';
    //window.history.back();
  } else {
    window.location = '/';
  };
};
function volverborrar() {
  console.log(origen);
  if (origen == 'a') {
    window.location = '/admin';
  } else {
    window.location = '/';
  };
};

function volverdetalle() {
  if (sessionStorage["Ventana"] == "admin") {
    window.location = '/admin';
  } else {
    if (sessionStorage["Ventana"] == "admin/detalle") {
        window.location = '/admin';
    } else {
      window.location = '/';
    }
  };
};

<!-- Ventana Borrar Posts Delete -->
function buscarDetallesPosts4(id) {
  origen = getQueryVariable('origen');
  console.log(origen);

  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == id)
    {
      // mostrar detalle
      document.getElementById("m1").innerHTML = "Titulo actual: " + posts[i].titulo;
      document.getElementById("m2").innerHTML = "Texto actual: " + posts[i].texto;

      if (posts[i].autor != undefined)
      {
        document.getElementById("h4").innerHTML = "Autor: " + posts[i].autor.nombre + " " + posts[i].autor.apellido;
      }
      else {
        document.getElementById("h4").innerHTML = "Autor: Anonimus"
      };
      break;
    }
  }
};
function obtenerTodosPosts(){
  sessionStorage["Filtro"] = "todosposts";
  sessionStorage["Filtro2"] = "0";
  sessionStorage["Filtro3"] = "0";
  sessionStorage["Pagina"] = 1;
  obtener6Posts();
};

function obtenerMisPosts(){
  sessionStorage["Filtro"] = "misposts";
  sessionStorage["Filtro2"] = "0";
  sessionStorage["Filtro3"] = "0";
  sessionStorage["Pagina"] = 1;
  obtener6Posts();
};

<!-- Obsoleto -->
function obtenerMisPosts2(){
  var URLUSU = 'q={"autor":{"nombre":"' + sessionStorage["usuario"] + '","apellido":"' + sessionStorage["usuario"] + '"}}'

  var URLItem2 = URLBASE2;
      URLItem2 += URLUSU;
      URLItem2 += apiKey2;
  console.log(URLItem2);
  //https://api.mlab.com/api/1/databases/bootcamp/collections/posts?q={"autor": "Bob"}&apiKey=xxxxxx

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem2, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  sessionStorage["Filtro"] = "misposts";
  mostrarPost();
};

//if (sessionStorage["Filtro"] == "todosposts") {
//  var URLItem3  = URLBASE2;
//      URLItem3 += URLCONT;
//      URLItem3 += apiKey2;
//} else {
//  var URLItem3  = URLBASE2;
//      URLItem3 += URLUSU;
//      URLItem3 += "&" + URLCONT;
//      URLItem3 += apiKey2;
//};
//
function obtener6Posts(){
  if (sessionStorage["Pagina"] == undefined) {
      sessionStorage["Pagina"] = 1;};
  var pagina = parseInt(sessionStorage["Pagina"]);
  var tiemponum = parseInt(sessionStorage["Filtro2"])+1;
  var Filtro3 =sessionStorage["Filtro3"];
  var URLFILINI ='q={'
  var URLFILFIN ='}'
  var URLUSU = '"autor":{"nombre":"' + sessionStorage["usuario"] + '","apellido":"' + sessionStorage["usuario"] + '"}';
  var URLFIL2 = '"tiempo":{"$lt": '+tiemponum+'}';
  var URLFIL3 = '"dificultad":"' + Filtro3 + '"';

  var URLCONT = 'c=true';
  var URLLIM = 'l=6';
  var URLPAG = "sk=" + ( (sessionStorage["Pagina"] -1) * 6);

  var URLItem3  = URLBASE2;
      URLItem3 = URLItem3 + URLFILINI;

  if (sessionStorage["Filtro"] == "misposts") {
      URLItem3 = URLItem3 + URLUSU;
      if (tiemponum > 1) {
        var URLItem3 = URLItem3 +"," + URLFIL2;
      };
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 +"," + URLFIL3;
      };
  } else {
    if (tiemponum > 1) {
      var URLItem3 = URLItem3 + URLFIL2;
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 +"," + URLFIL3;
      };
    } else {
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 + URLFIL3;
      }
    }
  };
  URLItem3 = URLItem3 + URLFILFIN;
  var URLItem2 = URLItem3;

  URLItem3 = URLItem3 + "&" + URLCONT + apiKey2;
  console.log("Cont6: "+URLItem3);
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem3, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["TotalPost"] = peticion.responseText;

  var totalpag = Math.ceil(sessionStorage["TotalPost"]/6);

  var pagsig = parseInt(pagina) + 1;
  var pagant = pagina - 1;
  console.log(totalpag); console.log(pagina);console.log(pagsig);console.log(pagant);
  if (totalpag > pagina) {
    btnSig.disabled = false;
  }   else {btnSig.disabled = true};
  if (pagant > 0) {btnAnt.disabled = false;}
  else {btnAnt.disabled = true;};

// con sk =

  URLItem2 = URLItem2 + "&"+ URLLIM + "&" + URLPAG  + apiKey2;

  console.log("Saco6: "+URLItem2);
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem2, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;

  document.getElementById("Totalpost").innerHTML = "Filtro: "+sessionStorage["Filtro"]+" - Pág: " + pagina + " de " + totalpag + " - TotalPost: " + sessionStorage["TotalPost"] + " .";
  mostrarPost();
};

function obtenerSigPosts(){
  var tiemponum = parseInt(sessionStorage["Filtro2"])+1;
  var Filtro3 =sessionStorage["Filtro3"];
  var URLFILINI ='q={'
  var URLFILFIN ='}'
  var URLUSU = '"autor":{"nombre":"' + sessionStorage["usuario"] + '","apellido":"' + sessionStorage["usuario"] + '"}';
  var URLFIL2 = '"tiempo":{"$lt": '+tiemponum+'}';

  var URLCONT = 'c=true';
  var URLLIM = 'l=6';
  var URLPAG = "sk=" + (sessionStorage["Pagina"] * 6);
  var URLItem3  = URLBASE2;
      URLItem3 = URLItem3 + URLFILINI;

  if (sessionStorage["Filtro"] == "misposts") {
      URLItem3 = URLItem3 + URLUSU;
      if (tiemponum > 1) {
        var URLItem3 = URLItem3 +"," + URLFIL2;
      };
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 +"," + URLFIL3;
      };
  } else {
    if (tiemponum > 1) {
      var URLItem3 = URLItem3 + URLFIL2;
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 +"," + URLFIL3;
      };
    } else {
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 + URLFIL3;
      }
    }
  };
  URLItem3 = URLItem3 + URLFILFIN;
  var URLItem2 = URLItem3;

  URLItem3 = URLItem3 + "&" + URLCONT + apiKey2;
  console.log(URLItem3);

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem3, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["TotalPost"] = peticion.responseText;

// con sk =
  URLItem2 = URLItem2 + "&"+ URLLIM + "&" + URLPAG  + apiKey2;

  console.log(URLItem2);
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem2, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;

  sessionStorage["Pagina"] = ++(sessionStorage["Pagina"]) ;

  var pagina = parseInt(sessionStorage["Pagina"]);
  var totalpag = Math.ceil(sessionStorage["TotalPost"]/6);
  var pagsig = parseInt(pagina) + 1;
  var pagant = pagina - 1;
  console.log(totalpag); console.log(pagina);console.log(pagsig);console.log(pagant);
  if (totalpag > pagina) {
    btnSig.disabled = false; }
  else {btnSig.disabled = true;};
  if (pagant > 0) {btnAnt.disabled = false;}
  else {btnAnt.disabled = true;};

  document.getElementById("Totalpost").innerHTML = "Filtro: "+sessionStorage["Filtro"]+" - Pág: " + pagina + " de " + totalpag + " - TotalPost: " + sessionStorage["TotalPost"] + " .";
  mostrarPost();
};

function obtenerAntPosts(){
  var tiemponum = parseInt(sessionStorage["Filtro2"])+1;
  var Filtro3 =sessionStorage["Filtro3"];
  var URLFILINI ='q={'
  var URLFILFIN ='}'
  var URLUSU = '"autor":{"nombre":"' + sessionStorage["usuario"] + '","apellido":"' + sessionStorage["usuario"] + '"}';
  var URLFIL2 = '"tiempo":{"$lt": '+tiemponum+'}';

  var URLCONT = 'c=true';
  var URLLIM = 'l=6';
  var URLPAG = "sk=" + ( (sessionStorage["Pagina"] -2) * 6)
  var URLItem3  = URLBASE2;
      URLItem3 = URLItem3 + URLFILINI;

  if (sessionStorage["Filtro"] == "misposts") {
      URLItem3 = URLItem3 + URLUSU;
      if (tiemponum > 1) {
        var URLItem3 = URLItem3 +"," + URLFIL2;
      };
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 +"," + URLFIL3;
      };
  } else {
    if (tiemponum > 1) {
      var URLItem3 = URLItem3 + URLFIL2;
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 +"," + URLFIL3;
      };
    } else {
      if (Filtro3 != "0") {
        var URLItem3 = URLItem3 + URLFIL3;
      }
    }
  };
  URLItem3 = URLItem3 + URLFILFIN;
  var URLItem2 = URLItem3;

  URLItem3 = URLItem3 + "&" + URLCONT + apiKey2;

  console.log(URLItem3);

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem3, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["TotalPost"] = peticion.responseText;

// con sk =
  URLItem2 = URLItem2 + "&"+ URLLIM + "&" + URLPAG  + apiKey2;
  console.log(URLItem2);

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URLItem2, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;

  sessionStorage["Pagina"] = --(sessionStorage["Pagina"]) ;

  var pagina = parseInt(sessionStorage["Pagina"]);
  var totalpag = Math.ceil(sessionStorage["TotalPost"]/6);
  var pagsig = parseInt(pagina) + 1;
  var pagant = pagina - 1;
  console.log(totalpag); console.log(pagina);console.log(pagsig);console.log(pagant);
  if (totalpag > pagina) {
    btnSig.disabled = false; }
  else {btnSig.disabled = true;};
  if (pagant > 0) {btnAnt.disabled = false;}
  else {btnAnt.disabled = true;};

  document.getElementById("Totalpost").innerHTML = "Filtro: "+sessionStorage["Filtro"]+" - Pág: " + pagina + " de " + totalpag + " - TotalPost: " + sessionStorage["TotalPost"] + " .";
  mostrarPost();
};

function ContarLogado(){
  var URLUSU = 'q={"autor":{"nombre":"' + sessionStorage["usuario"] + '","apellido":"' + sessionStorage["usuario"] + '"}}';
  var URLCONT = 'c=true';
  var URLItem3  = URLBASE2;
      URLItem3 += URLUSU;
      URLItem3 += "&" + URLCONT;
      URLItem3 += apiKey2;
  console.log(URLItem3)
  var peticion2 = new XMLHttpRequest();
  peticion2.open("GET", URLItem3, false);
  peticion2.setRequestHeader("Content-Type", "application/json");
  peticion2.send();
  respu = JSON.parse(peticion2.responseText);
  sessionStorage["Numero"] = peticion2.responseText;
  console.log(respu);
};

function hacerLogin()
{
    var nombre = document.getElementById("nombre").value;
    var password = document.getElementById("password").value;

    console.log(nombre);
    console.log(password);

    var peticion = new XMLHttpRequest();
    peticion.open("POST", URLPOSTGRE, false);
    peticion.setRequestHeader("Content-Type", "application/json");
    peticion.setRequestHeader("Access-Control-Allow-Origin", "*");
    peticion.send('{"nombre":"' + nombre + '", "password": "' + password + '"}');
    var spanMensaje = document.getElementById("mensaje");
    var respuesta = JSON.parse(peticion.responseText);

    if (peticion.status=="200" && respuesta.data.count > 0)
    {
        spanMensaje.innerText = "Login correcto - El usuario podrá dar de alta Post";
        sessionStorage.setItem("usuario", nombre);
        sessionStorage["Filtro"] = "misposts";
        usuarios();
    }
    else
    {
        spanMensaje.innerText = "Login incorrecto. <Se eliminan usuarios anteriores>";
        alert ("Login incorrecto. <Se eliminan usuarios anteriores>");
        sessionStorage.removeItem("usuario");
        sessionStorage.removeItem("correo");
    }
};

function usuarios()
{
    var peticion = new XMLHttpRequest();
    peticion.open("GET", URLPOSTGRE2, false);
    peticion.setRequestHeader("Content-Type", "application/json");
    peticion.setRequestHeader("Access-Control-Allow-Origin", "*");
    peticion.send();
    response = JSON.parse(peticion.responseText);
    console.log(response);

    var spanMensaje = document.getElementById("mensaje");

    if (peticion.status=="200" )
    {
      sessionStorage["ListaUsuarios"] = peticion.responseText;
      datosusuario()
    }
    else
    {
      spanMensaje.innerText = "Error al obtener dato user.";
    }
};

function datosusuario()
{
    var user = sessionStorage["usuario"];
    var usus = JSON.parse(sessionStorage["ListaUsuarios"]);
    var spanMensaje = document.getElementById("mensaje");
    for (var i = 0; i < usus.data.length; i++) {
      if (usus.data[i].nombre == user)
      {
        // mostrar detalle
        spanMensaje.innerText = "Login y datos correctos - El usuario podrá dar de alta Post";
        sessionStorage["correo"] = usus.data[i].email;
        sessionStorage["idusu"] = usus.data[i].id;
        sessionStorage.removeItem("ListaUsuarios");

        alert("Login y datos correctos - El usuario podrá dar de alta Post. \n \n" + "Correo: " + sessionStorage["correo"] + " - " +"IdUsu: " + sessionStorage["idusu"]);
        break;
      }

    else
      {
        spanMensaje.innerText = "Error al obtener email del user.";
      }
    }
};
function crearUsuario()
{
    var nombre = document.getElementById("nombrereg").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("passwordreg").value;
    console.log(nombre);
    console.log(email);
    console.log(password);
    var peticion = new XMLHttpRequest();
    peticion.open("POST", URLPOSTGRE2, false);
    peticion.setRequestHeader("Content-Type", "application/json");
    peticion.setRequestHeader("Access-Control-Allow-Origin", "*");
    peticion.send('{"nombre":"' + nombre + '", "email": "' + email + '", "password": "' + password + '"}');
    var spanMensaje = document.getElementById("mensajereg");
    var respuesta = JSON.parse(peticion.responseText);
    if (peticion.status=="200")
    {
        spanMensaje.innerText = "Usuario creado correctamente";
        sessionStorage.setItem("usuario", nombre);
        sessionStorage.setItem("correo", email);
        sessionStorage.removeItem("idusu");
        sessionStorage["Filtro"] = "misposts";
        alert ("Usuario creado correctamente");
        window.location = '/';

    }
    else
    {
        spanMensaje.innerText = "No se pudo crear el usuario";
    }
};


function BorrarUsu() {
  var spanMensaje = document.getElementById("mensaje");
  spanMensaje.innerText = "<Se eliminan usuarios anteriores>";
  alert ("<Se eliminan usuarios anteriores>");
  sessionStorage.removeItem("usuario");
  sessionStorage.removeItem("correo");
  sessionStorage["Filtro"] = "todosposts";
  sessionStorage["Pagina"] = 1
  window.location = '/';
};

function filtros() {
  sessionStorage["Pagina"] = 1
  var eleusubusca = document.getElementsByName("usubusca");
  for(var i=0; i < eleusubusca.length; i++) {
    if (eleusubusca[i].checked) {
      sessionStorage["Filtro"] = eleusubusca[i].value;
    };
  };

  if (document.getElementById("tiempo").value > 0) {
    sessionStorage["Filtro2"]=document.getElementById("tiempo").value;
  } else {
      sessionStorage.removeItem("Filtro2");
  };

  sessionStorage.removeItem("Filtro3");
  var eledificultad = document.getElementsByName("dificultad");
  for(var i=0; i < eledificultad.length; i++) {
    if (eledificultad[i].checked) {
      sessionStorage["Filtro3"] = eledificultad[i].value;
    };
  };
};

function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0; i < vars.length; i++) {
       var pair = vars[i].split("=");
       if(pair[0] == variable) {
           return pair[1];
       }
   }
   return false;
};

function sinimagen1(imag) {
  document.getElementById(imag).src = sinimagen;
};
