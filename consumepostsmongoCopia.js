
const URL = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts?apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs"
const URLBASE = "https://api.mlab.com/api/1/databases/bootcamparmando/collections/posts/"
const apiKey = "?apiKey=MvBxyyp_2-bdeYABQt62Wux4U5Qe0eBs"
var response;

function obtenerPosts(){

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  mostrarPost();
};

function mostrarPost() {
  var tabla = document.getElementById("tablaPosts");
  for (var i = 0; i < response.length; i++) {
    //alert(response[i].titulo);
    var fila = tabla.insertRow(i+1);
    var celdaTitulo = fila.insertCell(0);
    var celdaTexto = fila.insertCell(1);
    var celdaAutor = fila.insertCell(2);
    var celdaId = fila.insertCell(3);
    var celdaOperaciones = fila.insertCell(4);

    celdaTitulo.innerHTML = response[i].titulo;
    celdaTexto.innerHTML = response[i].texto;
    if (response[i].autor != undefined)
    {
      celdaAutor.innerHTML = response[i].autor.nombre + " " + response[i].autor.apellido;
    }
    else {
      celdaAutor.innerHTML = "Anonimus"
    };
    celdaId.innerHTML = response[i]._id.$oid
    celdaOperaciones.innerHTML = '<button onclick=\'actualizarPost("' + celdaId.innerHTML + '")\';>Actualizar</button>'+
      '<button onclick=\'deletePost("' + celdaId.innerHTML + '")\';>Borrar</button>';
  }
};

function anadirPost() {
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Nuevo Post desde Atom", "texto":"Nuevo texto desde Atom", "autor":{"nombre":"Armandito", "apellido":"Casitas"}}');

};

function actualizarPost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = URLBASE;
      URLItem += id;
      URLItem += apiKey;
  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Titulo cambiad32"}');

};

function deletePost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = URLBASE;
      URLItem += id;
      URLItem += apiKey;
  peticion.open("DELETE", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
};

function seleccionarPost(numero){
  sessionStorage["seleccionado"]=numero;
};

function buscarDetallesPosts(numero) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == numero)
    {
      // mostrar detalle
      document.getElementById("h1").innerHTML = numero;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      document.getElementById("h3").innerHTML = posts[i].texto;

      break;
    }
  }
};
