'use strict';

const express = require('express');
// Definir funciones extras
const path = require('path');

//Constantes definidas
const PORT= 8081;

//App Aplicacion
const app = express();
app.use(express.static(__dirname));

app.get('/', function(req, res) {
  //res.send("Bienvenido al mundo\n");
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/detallePost/:id', function(req, res) {
  res.sendFile(path.join(__dirname+'/detallePost.html'));
});

app.get('/modificarPost/:id', function(req, res) {
  res.sendFile(path.join(__dirname+'/modificarPost.html'));
});

app.get('/anadirPost', function(req, res) {
  res.sendFile(path.join(__dirname+'/anadirPost.html'));
});

app.get('/deletePost/:id', function(req, res) {
  res.sendFile(path.join(__dirname+'/deletePost.html'));
});

app.get('/admin', function(req, res) {
  res.sendFile(path.join(__dirname+'/admin.html'));
});

app.get('/estadis', function(req, res) {
  res.sendFile(path.join(__dirname+'/estadis.html'));
});

app.get('/login', function(req, res) {
  res.sendFile(path.join(__dirname+'/login.html'));
});

app.get('/registro', function(req, res) {
  res.sendFile(path.join(__dirname+'/registro.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto"   + PORT);

// Añado servidor.js

var http = require('http');
var url = require('url');
var fs = require('fs');

http.createServer(function(req, res)
{
    var q = url.parse(req.url, true);
    var filename = "." + q.pathname;
    fs.readFile(filename, function(err, data) {
        if(err) {
            res.writeHead(404, {'Content-Type':'text/html'});
            return res.end("404 No encontrado");
        }
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
    });
});
// }).listen(8080);
